package ru.tsc.fuksina.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.api.service.IReceiverService;
import ru.tsc.fuksina.tm.listener.LoggerListener;
import ru.tsc.fuksina.tm.service.ReceiverService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LoggerListener loggerListener;

    @SneakyThrows
    public void init() {
        receiverService.receive(loggerListener);
    }

}
