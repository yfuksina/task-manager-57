package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.dto.event.OperationEvent;

public interface ISenderService {

    void send(@NotNull String message);

}
