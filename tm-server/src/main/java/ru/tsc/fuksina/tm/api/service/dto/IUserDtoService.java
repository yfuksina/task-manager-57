package ru.tsc.fuksina.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.UserDto;
import ru.tsc.fuksina.tm.enumerated.Role;

public interface IUserDtoService extends IServiceDto<UserDto> {

    @Nullable
    UserDto findOneByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    @Nullable
    UserDto findOneByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    void removeByLogin(@Nullable String login);

    @Nullable
    UserDto create(@Nullable String login, @Nullable String password);

    @Nullable
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    UserDto create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDto setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    UserDto updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
