package ru.tsc.fuksina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.api.repository.dto.IUserOwnedRepositoryDto;
import ru.tsc.fuksina.tm.api.service.dto.IServiceDto;
import ru.tsc.fuksina.tm.api.service.dto.IUserOwnedServiceDto;
import ru.tsc.fuksina.tm.dto.model.AbstractUserOwnedModelDto;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.exception.field.IdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractUserOwnedServiceDto<M extends AbstractUserOwnedModelDto, R extends IUserOwnedRepositoryDto<M>> extends AbstractServiceDto<M, R> implements IUserOwnedServiceDto<M>, IServiceDto<M> {

    @NotNull
    protected abstract IUserOwnedRepositoryDto<M> getRepository();

    @Override
    public void add(@Nullable final String userId, @NotNull M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public  M findOneById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @NotNull final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final String userId, @NotNull final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final IUserOwnedRepositoryDto<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

}
