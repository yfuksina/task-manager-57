package ru.tsc.fuksina.tm.api.repository.dto;

import ru.tsc.fuksina.tm.dto.model.ProjectDto;

public interface IProjectDtoRepository extends IUserOwnedRepositoryDto<ProjectDto> {
}
