package ru.tsc.fuksina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.UserLoginRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.event.ConsoleEvent;
import ru.tsc.fuksina.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "Login user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    @EventListener(condition = "@userLoginListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @Nullable final String token =  getAuthEndpoint().login(request).getToken();
        setToken(token);
        System.out.println(token);
    }

}
